import '@/assets/fonts/iconfont.css'
import HelloWorld from './components/HelloWorld';
import SayHello from './components/SayHello';
// import { Button, Drawer, Tooltip } from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css'
// TODO 在宿主环境引入element依赖

// 将引入的组件模块存储，方便循环注册所有组件
const components = [ HelloWorld, SayHello ];

const install = (Vue,options) => {
  if (install.installed) return;
  install.installed = true
  console.log(options)
  components.forEach(component => {
    Vue.component(component.name, component)
  })
}

// 如果是直接引入的
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default {
  // 使用Vue.use必须具有install方法
  // https://cn.vuejs.org/v2/api/#Vue-use
  install
}
